#!/bin/sh
LOCAL_DIR=$1
FILE_TYPE=$2
S3_BUCKET=$3
PROFILE=$4

echo "${LOCAL_DIR} to ${S3_BUCKET} by ${PROFILE}"

if [ "$FILE_TYPE" = "js" ] || [ "$FILE_TYPE" = "all" ]; then
  aws s3 sync $LOCAL_DIR $S3_BUCKET --exclude "*" --include "*.js" --acl public-read --cache-control "max-age=300" --content-type "application/javascript" --content-encoding "gzip" --profile ${PROFILE}
fi
if [ "$FILE_TYPE" = "css" ] || [ "$FILE_TYPE" = "all" ]; then
  aws s3 sync $LOCAL_DIR $S3_BUCKET --exclude "*" --include "*.css" --acl public-read --cache-control "max-age=300" --content-type "text/css" --content-encoding "gzip" --profile ${PROFILE}
fi
if [ "$FILE_TYPE" = "html" ] || [ "$FILE_TYPE" = "all" ]; then
  aws s3 sync $LOCAL_DIR $S3_BUCKET --exclude "*" --include "*.html" --acl public-read --cache-control "max-age=300" --content-type "text/html" --content-encoding "gzip" --profile ${PROFILE}
fi
if [ "$FILE_TYPE" = "txt" ] || [ "$FILE_TYPE" = "all" ]; then
  aws s3 sync $LOCAL_DIR $S3_BUCKET --exclude "*" --include "*.txt" --acl public-read --cache-control "max-age=300" --content-type "text/plain" --content-encoding "gzip" --profile ${PROFILE}
fi
if [ "$FILE_TYPE" = "pdf" ] || [ "$FILE_TYPE" = "all" ]; then
  aws s3 sync $LOCAL_DIR $S3_BUCKET --exclude "*" --include "*.pdf" --acl public-read --cache-control "max-age=300" --content-type "application/pdf  " --content-encoding "gzip" --profile ${PROFILE}
fi
if [ "$FILE_TYPE" = "jpg" ] || [ "$FILE_TYPE" = "all" ]; then
  aws s3 sync $LOCAL_DIR $S3_BUCKET --exclude "*" --include "*.jpg" --acl public-read --cache-control "max-age=300" --content-type "image/jpeg" --content-encoding "gzip" --profile ${PROFILE}
fi
if [ "$FILE_TYPE" = "png" ] || [ "$FILE_TYPE" = "all" ]; then
  aws s3 sync $LOCAL_DIR $S3_BUCKET --exclude "*" --include "*.png" --acl public-read --cache-control "max-age=300" --content-type "image/png" --content-encoding "gzip" --profile ${PROFILE}
fi
if [ "$FILE_TYPE" = "gif" ] || [ "$FILE_TYPE" = "all" ]; then
  aws s3 sync $LOCAL_DIR $S3_BUCKET --exclude "*" --include "*.gif" --acl public-read --cache-control "max-age=300" --content-type "image/gif" --content-encoding "gzip" --profile ${PROFILE}
fi
if [ "$FILE_TYPE" = "woff" ] || [ "$FILE_TYPE" = "all" ]; then
  aws s3 sync $LOCAL_DIR $S3_BUCKET --exclude "*" --include "*.woff" --acl public-read --cache-control "max-age=300" --content-type "application/font-woff" --content-encoding "gzip" --profile ${PROFILE}
fi
if [ "$FILE_TYPE" = "woff2" ] || [ "$FILE_TYPE" = "all" ]; then
  aws s3 sync $LOCAL_DIR $S3_BUCKET --exclude "*" --include "*.woff2" --acl public-read --cache-control "max-age=300" --content-type "application/font-woff2" --content-encoding "gzip" --profile ${PROFILE}
fi
if [ "$FILE_TYPE" = "ttf" ] || [ "$FILE_TYPE" = "all" ]; then
  aws s3 sync $LOCAL_DIR $S3_BUCKET --exclude "*" --include "*.ttf" --acl public-read --cache-control "max-age=300" --content-type "application/x-font-truetype" --content-encoding "gzip" --profile ${PROFILE}
fi
if [ "$FILE_TYPE" = "ico" ] || [ "$FILE_TYPE" = "all" ]; then
  aws s3 sync $LOCAL_DIR $S3_BUCKET --exclude "*" --include "*.ico" --acl public-read --cache-control "max-age=300" --content-type "image/vnd.microsoft.icon" --content-encoding "gzip" --profile ${PROFILE}
fi
if [ "$FILE_TYPE" = "swf" ] || [ "$FILE_TYPE" = "all" ]; then
  aws s3 sync $LOCAL_DIR $S3_BUCKET --exclude "*" --include "*.swf" --acl public-read --cache-control "max-age=300" --content-type "application/x-shockwave-flash" --content-encoding "gzip" --profile ${PROFILE}
fi
if [ "$FILE_TYPE" = "xml" ] || [ "$FILE_TYPE" = "all" ]; then
  aws s3 sync $LOCAL_DIR $S3_BUCKET --exclude "*" --include "*.xml" --acl public-read --cache-control "max-age=300" --content-type "application/xml" --content-encoding "gzip" --profile ${PROFILE}
fi
if [ "$FILE_TYPE" = "svg" ] || [ "$FILE_TYPE" = "all" ]; then
  aws s3 sync $LOCAL_DIR $S3_BUCKET --exclude "*" --include "*.svg" --acl public-read --cache-control "max-age=300" --content-type "image/svg+xml" --content-encoding "gzip" --profile ${PROFILE}
fi
if [ "$FILE_TYPE" = "mov" ] || [ "$FILE_TYPE" = "all" ]; then
  aws s3 sync $LOCAL_DIR $S3_BUCKET --exclude "*" --include "*.mov" --acl public-read --cache-control "max-age=300" --content-type "video/quicktime" --profile ${PROFILE}
fi
if [ "$FILE_TYPE" = "m4v" ] || [ "$FILE_TYPE" = "all" ]; then
  aws s3 sync $LOCAL_DIR $S3_BUCKET --exclude "*" --include "*.m4v" --acl public-read --cache-control "max-age=300" --content-type "video/mp4" --profile ${PROFILE}
fi
