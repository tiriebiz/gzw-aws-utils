#!/bin/sh
# Usage : ./deploy_s3.sh ${DOCUMENT_ROOT} ${FILE_TYPE} ${S3_BUCKET} ${PROFILE}

DOCUMENT_ROOT=`pwd`/$1
GZIP_ROOT=$DOCUMENT_ROOT/../gzip
FILE_TYPE=$2
S3_BUCKET=$3
PROFILE=$4

SH_DIR=`dirname $0`
cd ${SH_DIR}

GZIP_SRC_SH=./_gzip_src.sh
SYNC_SH=./_s3sync.sh

echo "$DOCUMENT_ROOT to $GZIP_ROOT"

rm -rdf $GZIP_ROOT
cp -R $DOCUMENT_ROOT $GZIP_ROOT

if [ "$FILE_TYPE" = "txt" ] || [ "$FILE_TYPE" = "all" ]; then
  find $GZIP_ROOT -name "*.txt" -exec $GZIP_SRC_SH '{}' \;
else
  echo "type: $FILE_TYPE"
fi
if [ "$FILE_TYPE" = "js" ] || [ "$FILE_TYPE" = "all" ]; then
  find $GZIP_ROOT -name "*.js" -exec $GZIP_SRC_SH '{}' \;
fi
if [ "$FILE_TYPE" = "html" ] || [ "$FILE_TYPE" = "all" ]; then
  find $GZIP_ROOT -name "*.html" -exec $GZIP_SRC_SH '{}' \;
fi
if [ "$FILE_TYPE" = "css" ] || [ "$FILE_TYPE" = "all" ]; then
  find $GZIP_ROOT -name "*.css" -exec $GZIP_SRC_SH '{}' \;
fi
if [ "$FILE_TYPE" = "jpg" ] || [ "$FILE_TYPE" = "all" ]; then
  find $GZIP_ROOT -name "*.jpg" -exec $GZIP_SRC_SH '{}' \;
fi
if [ "$FILE_TYPE" = "png" ] || [ "$FILE_TYPE" = "all" ]; then
  find $GZIP_ROOT -name "*.png" -exec $GZIP_SRC_SH '{}' \;
fi
if [ "$FILE_TYPE" = "gif" ] || [ "$FILE_TYPE" = "all" ]; then
  find $GZIP_ROOT -name "*.gif" -exec $GZIP_SRC_SH '{}' \;
fi
if [ "$FILE_TYPE" = "pdf" ] || [ "$FILE_TYPE" = "all" ]; then
  find $GZIP_ROOT -name "*.pdf" -exec $GZIP_SRC_SH '{}' \;
fi
if [ "$FILE_TYPE" = "ico" ] || [ "$FILE_TYPE" = "all" ]; then
  find $GZIP_ROOT -name "*.ico" -exec $GZIP_SRC_SH '{}' \;
fi
if [ "$FILE_TYPE" = "swf" ] || [ "$FILE_TYPE" = "all" ]; then
  find $GZIP_ROOT -name "*.swf" -exec $GZIP_SRC_SH '{}' \;
fi
if [ "$FILE_TYPE" = "woff" ] || [ "$FILE_TYPE" = "all" ]; then
  find $GZIP_ROOT -name "*.woff" -exec $GZIP_SRC_SH '{}' \;
fi
if [ "$FILE_TYPE" = "woff2" ] || [ "$FILE_TYPE" = "all" ]; then
  find $GZIP_ROOT -name "*.woff2" -exec $GZIP_SRC_SH '{}' \;
fi
if [ "$FILE_TYPE" = "ttf" ] || [ "$FILE_TYPE" = "all" ]; then
  find $GZIP_ROOT -name "*.ttf" -exec $GZIP_SRC_SH '{}' \;
fi
if [ "$FILE_TYPE" = "xml" ] || [ "$FILE_TYPE" = "all" ]; then
  find $GZIP_ROOT -name "*.xml" -exec $GZIP_SRC_SH '{}' \;
fi
if [ "$FILE_TYPE" = "svg" ] || [ "$FILE_TYPE" = "all" ]; then
  find $GZIP_ROOT -name "*.svg" -exec $GZIP_SRC_SH '{}' \;
fi
if [ "$FILE_TYPE" = "mov" ] || [ "$FILE_TYPE" = "all" ]; then
  #find $GZIP_ROOT -name "*.mov" -exec $GZIP_SRC_SH '{}' \;
  echo 'mov file is not gzipped.';
fi
if [ "$FILE_TYPE" = "m4v" ] || [ "$FILE_TYPE" = "all" ]; then
  #find $GZIP_ROOT -name "*.mov" -exec $GZIP_SRC_SH '{}' \;
  echo 'm4v file is not gzipped.';
fi

$SYNC_SH $GZIP_ROOT $FILE_TYPE $S3_BUCKET $PROFILE
