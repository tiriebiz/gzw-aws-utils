#!/bin/sh
SRC=$1

gzip -9 $SRC
mv $SRC.gz $SRC

echo "$SRC gzipped."
