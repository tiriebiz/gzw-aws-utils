#!/bin/sh
ln -sf /usr/share/zoneinfo/Asia/Tokyo /etc/localtime
cat << EOF > /etc/sysconfig/clock 
ZONE="Asia/Tokyo"
UTC=true
EOF

