#!/bin/sh

PROFILE=$2
source_dist_id=$1
unixtime=$(date +%s)

aws cloudfront get-distribution --id ${source_dist_id} --profile ${PROFILE} | jq -r '.Distribution | .DistributionConfig.CallerReference = "'${unixtime}'" | del(.Status, .DomainName, .InProgressInvalidationBatches, .ActiveTrustedSigners, .LastModifiedTime, .Id)'

