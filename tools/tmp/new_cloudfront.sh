#!/bin/sh

PROFILE=$2
TEMPLATE_FILE=$1

echo "aws --profile ${PROFILE} cloudfront create-distribution --cli-input-json `cat ${TEMPLATE_FILE} | jq .`"
aws --profile ${PROFILE} cloudfront create-distribution --cli-input-json "`cat ${TEMPLATE_FILE} | jq .`"
